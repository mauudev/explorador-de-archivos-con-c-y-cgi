#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#define MAXLEN 1024
/*
 * Explorador de archivos en C
 * autor: Mauricio Trigo Uriona
 * */
 
void copiar(char *path, char *name, char *destino);
void separar(char *cadena, char *linea, char separador);
void reemplazar(char *cadena, char *resultado);
void path_anterior(char *ruta, char *ultimo);
int copy_cmd(char* cmd);

int main(void){

    char *lenstr;
    char inputBuffer[MAXLEN];
    int contentLength;
    int i;
    char x;    
    lenstr = getenv("CONTENT_LENGTH");
    if (lenstr!= NULL)
		contentLength = atoi(lenstr);   
    else contentLength = 0;

    if (contentLength > sizeof(inputBuffer)-1)
		contentLength = sizeof(inputBuffer)-1;

    i = 0;

    while (i < contentLength){
		x = fgetc(stdin);
		if (x==EOF) break;
		inputBuffer[i] = x;
		i++;
    }

    inputBuffer[i] = '\0';
    contentLength = i;

	char path[1024];
	char name[256];
	char destino[1024];
	separar(path, inputBuffer, '=');
    separar(path, inputBuffer, '&');

	separar(name, inputBuffer, '=');
    separar(name, inputBuffer, '&');
	
	separar(destino, inputBuffer, '=');
    separar(destino, inputBuffer, '&');

	char path2[1024];
	reemplazar(path,path2);
	char destino2[1024];
	reemplazar(destino,destino2);

	copiar(path2, name, destino2);

	return 0;
}

void copiar(char *path, char *name, char *destino){
	printf("Content-type: text/html\n\n");
	printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de Archivos</title>\n");
    printf("</head>\n");
    printf("<body background='/static/fondo.png'>\n");	
	char path2[2048];
	strcpy(path2,path);		
	char name2[1024];//dato para acceder ala penultima carpeta
	path_anterior(path2,name2);	
	char destino2[1024];
	strcpy(destino2,destino);	
	char file_name[1024];
	strcpy(file_name,name);	
	if(strstr(destino,"+") != NULL || strstr(destino,"%") != NULL || strstr(destino,"@") != NULL){//eliminar caracteres basura 
		int tam = strlen(destino);	
		int i;
		char new_destino[1024];
		int k = 0;
		for(i = 0; i<tam; i++){
			if(destino[i] == '+' || destino[i] == '%' || destino[i] == '@'){
				new_destino[k] = destino[i+1];
				k++;
				}else{ 
					new_destino[k] = destino[i];
					k++;
				}
			}
		new_destino[k] = '\0';
		char copy_cmd[1024] = "cp -R ";
		strcat(copy_cmd,path2);
		strcat(copy_cmd,"/");
		strcat(copy_cmd,file_name);
		strcat(copy_cmd," ");
		strcat(copy_cmd,new_destino);
		system(copy_cmd);
	}else{
		char copy_cmd[1024] = "cp -R ";
		strcat(copy_cmd,path2);
		strcat(copy_cmd,"/");
		strcat(copy_cmd,file_name);
		strcat(copy_cmd," ");
		strcat(copy_cmd,destino2);
		system(copy_cmd);
	}
	
    printf("<img alt="" height='40' src='/static/sucess.png' width='40' align='middle'/><a style='color:black;font-style:verdana;font-size:x-large'><strong>Se ha copiado correctamente..</strong></a>\n");
    printf("<a style=color:black;font-size:large;font-size:oblique;><form action=\"/cgi-bin/script_4.cgi\" method=\"post\">\n");
    printf("<a style=color:black;font-size:large;font-size:oblique;><input type=\"hidden\" value=\"%s\" name=\"path\">\n",path2);
	printf("<a style=color:black;font-size:large;font-size:oblique;><input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",file_name);
    printf("<a style=color:black;font-size:large;font-size:oblique;><input type=\"hidden\" value=\"1\" name=\"operacion\">\n");
    printf("<br>");
    printf("<a style=color:black;font-size:large;font-size:oblique;><input type=\"submit\" value=\"Regresar al directorio\">\n");
    printf("</form>\n");
    printf("</body>\n");
    printf("<br>");
    printf("<br>");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("<td><img alt="" height='40' src='/static/home.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/index.html\" style=font-family:verdana;color:black;>Portada</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");
	printf("</html>\n");
}


void separar(char *cadena, char *linea, char separador){
    int x, y; 
    x = 0; y = 0;
    while ((linea[x]) && (linea[x] != separador)){
		cadena[x] = linea[x];
		x = x + 1;
    }
    cadena[x] = '\0';
    if (linea[x]) ++x;  
    y = 0; 
    while (linea[y] = linea[x]){
		linea[y] = linea[x];
		y++;
		x++;
    }
}

void reemplazar(char *cadena, char *resultado){
    char *separador = "%2F";
    char *remplazo = "/";
    int i =0;
    int j =0;
    for(i=0;i<(strlen(cadena));i++){
        if( (cadena[i] == separador[0]) && (cadena[i+1] == separador[1]) && (cadena[i+2] == separador[2]) ){
                resultado[j] = '/';
                i = i+2;
        }else{
            resultado[j] = cadena[i];
        }
        j = j+1;
    }
    resultado[j] = '\0';
}


void path_anterior(char *ruta, char *ultimo){
    int tam = strlen(ruta)-1;
    int pos = 0;
    int puntero = 0;
    for(pos=tam;pos>=0;pos--){
        if(ruta[pos]=='/'){
            puntero = pos;
            pos = -6;
        }
    }
    int i = 0;
    char ruta2[1024];
    for(i;i<puntero;i++){
        ruta2[i]=ruta[i];
    }
    ruta2[i]='\0';
    char ultimo2[1024];
    int j = puntero+1;
    int k = 0;
    for(j;j<strlen(ruta);j++){
        ultimo2[k]=ruta[j];
        k++;
    }
    ultimo2[k] = '\0';
    strcpy(ruta,ruta2);
    strcpy(ultimo,ultimo2);
}
