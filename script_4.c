#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#define MAXLEN 1024
/*
 * Explorador de archivos en C
 * autor: Mauricio Trigo Uriona
 * */
void print_html_op_1(char *dato);
void print_html_op_2(char *path, char *name, char *operation);
void print_html_op_3(char *path, char *name, char *operation);
void print_html_op_4(char *parh, char *name, char *operation);
void print_html_op_5(char *path, char *name, char *operation);
void print_form(char *dato, char *ruta);
void err_quit(char *msg);
void listar_datos(char *ruta);
void separar(char *cadena, char *linea, char separador);
void reemplazar(char *cadena, char *resultado);
void *replace_str(char *array,char *str, char *orig, char *rep);

int main(void){
//--------->Recoleccion de datos del formulario
	char *lenstr;
    char inputBuffer[MAXLEN];
    int contentLength;
    int i;
    char x;    

    lenstr = getenv("CONTENT_LENGTH");
    if (lenstr!= NULL)
		contentLength = atoi(lenstr);   
    else contentLength = 0;

    if (contentLength > sizeof(inputBuffer)-1)
		contentLength = sizeof(inputBuffer)-1;

    i = 0;

    while (i < contentLength){
		x = fgetc(stdin);
		if (x==EOF) break;
		inputBuffer[i] = x;
		i++;
    }

    inputBuffer[i] = '\0';
    contentLength = i;

	char path[1024];
	char name[1024];
	char operation[1024];
    separar(path, inputBuffer, '=');
    separar(path, inputBuffer, '&');

    separar(name, inputBuffer, '=');
    separar(name, inputBuffer, '&');

    separar(operation, inputBuffer, '=');
    separar(operation, inputBuffer, '&');

//---------->Fin recoleccion de datos del form
	char path2[1024];
	reemplazar(path, path2);
	if(atoi(operation)==1){
		print_html_op_1(path2);
	}
	if(atoi(operation)==2){
		print_html_op_2(path2, name, operation);
	}
	if(atoi(operation)==3){
		print_html_op_3(path2, name, operation);
	}
	if(atoi(operation)==4){
		print_html_op_4(path2, name, operation);
	}
	if(atoi(operation)==5){
		print_html_op_5(path2, name, operation);
	}
    return 0;
}


void separar(char *cadena, char *linea, char separador){
    int x, y;
    x = 0; y = 0;
    while ((linea[x]) && (linea[x] != separador)){
		cadena[x] = linea[x];
		x = x + 1;
    }
    cadena[x] = '\0';
    if (linea[x]) ++x;
    y = 0;  
    while (linea[y] = linea[x]){
		linea[y] = linea[x];
		y++;
		x++;
    }
}
void print_html_op_1(char *dato){ //abrir
	printf("Content-type: text/html\n\n");
    printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de Archivos</title>\n");
    printf("</head>\n");
    printf("<body background='/static/fondo.png'>\n");	
	printf("<h1 style=font-family:verdana;color:black;>Explorador de Archivos</h1><br>");
	printf("<h2 style=font-style:oblique;color:black;>Directorio actual: %s<br>",dato);
	printf("<br>");
	listar_datos(dato);
    printf("</body>\n");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("<td><img alt="" height='40' src='/static/home.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/index.html\" style=font-family:verdana;color:black;>Portada</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");

}

void print_form(char *dato, char *ruta){ 
        printf("<form action=\"/cgi-bin/script_4.cgi\" method=\"post\">\n");
		//Agregar datos ocultos al formulario	
		int j;
		int k = 0;	
		int tam = strlen(ruta);
		char new_ruta[tam];

		if(ruta[tam-1] == '/')ruta[tam-1] = '\0';
		for(j = 0; j < tam; j++){
			if(ruta[j] != '.'){
				new_ruta[k] = ruta[j];
				k++;
				}
			}
		new_ruta[tam] = '\0';
		//**
		int m = tam;
		k = 0;
		int r;
		int flag = 0;
		char nivel_atras[tam];
		int x;
		if(strcmp(dato,"..")==0){
			while(flag == 0){
				if(new_ruta[m] == '/'){
					for(r = m; r >= 0; r--){
						k++;
						}
						flag = 1;
					}
				m--;
				}
			for(x = 0; x < k; x++){
				nivel_atras[x] = new_ruta[x];
			}
		}
		nivel_atras[x] = '\0';
		int tam2 = strlen(new_ruta);
		if(strcmp(dato,"..")==0)
			printf("<input type=\"hidden\" value=\"%s\" name=\"path\">\n",nivel_atras);
		else{
			strcat(new_ruta,"/");
			strcat(new_ruta,dato);
			printf("<input type=\"hidden\" value=\"%s\" name=\"path\">\n",new_ruta);
		}
		printf("<input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",dato);
		//Fin Agregacion de datos ocultos
		
		if (strstr(dato, ".") !=NULL ) {
			if(strcmp(dato, "..") == 0)
				printf("<h6>Directorio: %s</h6>",dato);
			else printf("<h6>Archivo: %s</h6>",dato);
		}else printf("<h6>Directorio: %s</h6>",dato);
		//printf("<br>");
		printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
        printf("<tr>\n");
        if (strstr(dato, ".") != NULL) {
			if(strcmp(dato, "..") == 0) // es directorio
				printf("<td><img alt="" height='40' src='/static/directorio.png' width='40' align='middle'/></td>\n");
			else printf("<td><img alt="" height='40' src='/static/fichero.png' width='40' align='middle'/></td>\n");
		}else printf("<td><img alt="" height='40' src='/static/directorio.png' width='40' align='middle'/></td>\n");
        printf("<td><center><h4 style='color:black'>/%s</h4></td></center>\n",dato);
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"1\">Abrir</td>");
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"2\">Eliminar</td>");
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"5\">Mover</td>");	
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"3\">Copiar</td>");	
        if (strstr(dato, ".") == NULL) 
			printf("<td><input type=\"radio\" name=\"operacion\" value=\"4\">Crear archivo o directorio</td>");
        printf("<td><input type=\"submit\" value=\"OK\">\n");
        printf("</tr>\n");
        printf("</table>\n");
        printf("</form>\n");
}


void listar_datos(char *ruta){
	DIR *dir;
    struct dirent *mydirent;
    int i = 1;    
    
    if((dir = opendir(ruta)) == NULL)
		err_quit("opendir");

    while((mydirent = readdir(dir)) != NULL){ 	
		char *name = mydirent->d_name;
		print_form(name, ruta);
    }
    closedir(dir);
    //exit(EXIT_SUCCESS);
}

void err_quit(char *msg){
    perror(msg);
    exit(EXIT_FAILURE);
}

void reemplazar(char *cadena, char *resultado){
    char *separador = "%2F";
    char *remplazo = "/";
    int i =0;
    int j =0;
    for(i=0;i<(strlen(cadena));i++){
        if( (cadena[i] == separador[0]) && (cadena[i+1] == separador[1]) && (cadena[i+2] == separador[2]) ){
                resultado[j] = '/';
                i = i+2;
        }else{
            resultado[j] = cadena[i];
        }
        j = j+1;
    }
    resultado[j] = '\0';
}


void print_html_op_2(char *path, char *name, char *operation){//eliminar
	printf ("Content-type:text/html\n\n");	
	printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de Archivos</title>\n");
    printf("</head>\n");
	printf("<body background='/static/fondo.png'>\n");	
	//Inicio del cuerpo del html
	printf("<br>");
	printf("<img alt="" height='40' src='/static/warning.png' width='40' align='left'/><a style=color:black;font-style:oblique;font-size:x-large;><strong>Se eliminara el siguiente elemento: %s</strong></a><br>\n",name);
	printf("<a style=color:black;font-style:oblique;font-size:medium;>Esta operacion NO se puede deshacer</a><br>\n");
	printf("<form action=\"/cgi-bin/script_5.cgi\" method=\"post\">\n");
	//Inicio Carga de datos ocultos
	printf("<input type=\"hidden\" value=\"%s\" name=\"path\">\n",path);
	printf("<input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",name);
	printf("<input type=\"hidden\" value=\"%s\" name=\"operation\">\n",operation);
	//Final  Carga de datos ocultos
	printf("<br>");
	printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><a style=color:black;><input type=\"radio\" name=\"decision\" value=\"si\">Eliminar</td>\n");
    printf("<td><a style=color:black;font-size:large;><input type=\"radio\" name=\"decision\" value=\"no\">Cancelar</td>\n");
	printf("</tr>\n");
    printf("</table>\n");
	printf("<br>");
	printf("<input type=\"submit\" value=\"Confirmar\"><br>\n");
	printf("</form>\n");
	//Fin    del cuerpo del html
	printf ("</body>\n");
	printf ("<br>");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("<td><img alt="" height='40' src='/static/home.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/index.html\" style=font-family:verdana;color:black;>Portada</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");
	
}

void print_html_op_3(char *path, char *name, char *operation){//copiar
	printf("Content-type: text/html\n\n");
	printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de archivos</title>\n");
    printf("</head>\n");
    printf("<body background='/static/fondo.png'>\n");	
    printf("<form action=\"/cgi-bin/script_6.cgi\" method=\"post\">\n");
    printf("<br>");
	printf("<img alt="" height='40' src='/static/copy_move.png' width='40' align='left'/><a style=color:black;font-style:oblique;font-size:x-large;><strong>Copiar archivo o directorio..</strong></a><br>\n");
	printf("<a style=color:black;font-style:oblique;font-size:medium;>Ingrese la ruta donde desea copiar %s:</a><br>\n",name);
	printf("<br>");
    printf("<input type=\"hidden\" value=\"%s\" name=\"path\">\n",path);
	printf("<input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",name);
    printf("<input type=\"text\" name=\"destino\" size=\"40\"><br><br>\n");
    printf("<input type=\"submit\" value=\"Copiar Aqui\">\n");
    printf("</form>\n");
    printf("</body>\n");
    printf("<br>");
    printf("<br>");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("<td><img alt="" height='40' src='/static/home.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/index.html\" style=font-family:verdana;color:black;>Portada</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");
    printf("</html>\n");
}
void print_html_op_4(char *path, char *name, char *operation){//crear archivo
	printf("Content-type: text/html\n\n");
	printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de archivos</title>\n");
    printf("</head>\n");
    printf("<body background='/static/fondo.png'>\n");	
    printf("<br>");
	printf("<img alt="" height='40' src='/static/new_file.ico' width='40' align='left'/><a style=color:black;font-style:oblique;font-size:x-large;><strong>Crear archivo o directorio </strong></a><br>\n");
    printf("<form action=\"/cgi-bin/script_3.cgi\" method=\"post\">\n");
    printf("<a style=color:black;font-style:oblique;font-size:medium;>Ingrese el nombre del archivo o directorio:</a><br><br>\n");
	printf("<input type=\"hidden\" value=\"%s\" name=\"path\">\n",path);
	printf("<input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",name);
    printf("<input type=\"text\" name=\"destino\" size=\"40\"><br><br>\n");
	printf("<input type=\"submit\" value=\"Crear Archivo\">\n");
	printf("<input type=\"submit\" value=\"Crear Directorio\">\n");
    printf("</form>\n");
    printf("</body>\n");
    printf ("<br>");
    printf("<br>");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");
    printf("</html>\n");
}

void print_html_op_5(char *path, char *name, char *operation){//mover
	printf("Content-type: text/html\n\n");
	printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de archivos</title>\n");
    printf("</head>\n");
    printf("<body background='/static/fondo.png'>\n");	
    printf("<form action=\"/cgi-bin/script_7.cgi\" method=\"post\">\n");
    printf("<br>");
	printf("<img alt="" height='40' src='/static/copy_move.png' width='40' align='left'/><a style=color:black;font-style:oblique;font-size:x-large;><strong>Mover archivo o directorio..</strong></a><br>\n");
	printf("<a style=color:black;font-style:oblique;font-size:medium;>Ingrese la ruta donde desea mover %s:</a><br>\n",name);
	printf("<br>");
    printf("<input type=\"hidden\" value=\"%s\" name=\"path\">\n",path);
	printf("<input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",name);
    printf("<input type=\"text\" name=\"destino\" size=\"40\"><br><br>\n");
    printf("<input type=\"submit\" value=\"Mover Aqui\">\n");
    printf("</form>\n");
    printf("</body>\n");
    printf("<br>");
    printf("<br>");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
	printf("<tr>\n");
	printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("<td><img alt="" height='40' src='/static/home.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/index.html\" style=font-family:verdana;color:black;>Portada</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");
    printf("</html>\n");
}
void *replace_str(char *array,char *str, char *orig, char *rep){
  char *p;
  if(!(p = strstr(str, orig)))  // Is 'orig' even in 'str'?
    return str;

  strncpy(array, str, p-str); // Copy characters from 'str' start to 'orig' st$
  array[p-str] = '\0';

  sprintf(array+(p-str), "%s%s", rep, p+strlen(orig));

}
