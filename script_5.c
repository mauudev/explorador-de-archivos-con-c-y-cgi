#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#define MAXLEN 1024

void separar(char *cadena, char *linea, char separador);
void print_conf_2(char *path, char *name, char *operation);
void print_conf_1(char *path, char *name, char *operation);
void reemplazar(char *cadena, char *resultado);
void last_clean(char *var1, char *var2);
void get_path_anterior(char *ruta, char *ultimo);
void listar_datos(char *ruta);
void print_form(char *dato, char *ruta);
void err_quit(char *msg);

int main(void){

	char *lenstr;
    char inputBuffer[MAXLEN];
    int contentLength;
    int i;
    char x;    
    lenstr = getenv("CONTENT_LENGTH");

    if (lenstr!= NULL)
		contentLength = atoi(lenstr);   
    else contentLength = 0;

    if (contentLength > sizeof(inputBuffer)-1)
		contentLength = sizeof(inputBuffer)-1;

    i = 0;

    while (i < contentLength){
		x = fgetc(stdin);
		if (x==EOF) break;
			inputBuffer[i] = x;
	 	i++;
    }

    inputBuffer[i] = '\0';
    contentLength = i;

/*Inicio de extraccion de datos*/
	char path[1024];
	char name[256];
	char operation[256];
	char decision[256];
	separar(path, inputBuffer, '=');
    separar(path, inputBuffer, '&');

	separar(name, inputBuffer, '=');
    separar(name, inputBuffer, '&');

	separar(operation, inputBuffer, '=');
    separar(operation, inputBuffer, '&');

	separar(decision, inputBuffer, '=');
    separar(decision, inputBuffer, '&');
    	

/*Fina de extraccion de datos*/
	char path2[1024];
	reemplazar(path, path2);
	
	if(strcmp(decision,"si")==0)
		print_conf_1(path2, name,operation);
	
	if(strcmp(decision,"no")==0)
		print_conf_2(path2,name,operation);	
		
	return 0;
}

void print_conf_1(char *path, char *name, char *operation){
	printf("Content-type: text/html\n\n");
	printf("\n");
	printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de Archivos</title>\n");
    printf("</head>\n");
    printf("<body background='/static/fondo.png'>\n");	
	char path_nuevo[2048];
	strcpy(path_nuevo,path);	
	char file_name[2048];
	strcpy(file_name,name);
	char ultimo3[1024];
	get_path_anterior(path_nuevo,ultimo3);
	printf("<br>");    
	printf("<img alt="" height='40' src='/static/sucess.png' width='40' align='middle'/><a style=color:black;font-style:oblique;font-size:x-large;><strong>Se ha eliminado el siguiente elemento: %s</strong></a><br>\n",name);
    printf("<form action=\"/cgi-bin/script_4.cgi\" method=\"post\">\n");
    printf("<br>");    
	printf("<a style=color:black;><input type=\"hidden\" value=\"%s\" name=\"path\">\n",path_nuevo);
	printf("<a style=color:black;><input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",file_name);
	printf("<a style=color:black;><input type=\"hidden\" value=\"1\" name=\"operacion\">\n");
    printf("<a style=color:black;><input type=\"submit\" value=\"Regresar al directorio\">\n");
	char comando_eliminar[1024] = "rm -R ";
	strcat(comando_eliminar,path_nuevo);
	strcat(comando_eliminar,"/");
	strcat(comando_eliminar,file_name);
	system(comando_eliminar);
    printf("</form>\n");
    printf("</body>\n");
    printf("<br>");
    printf("<br>");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("<td><img alt="" height='40' src='/static/home.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/index.html\" style=font-family:verdana;color:black;>Portada</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");
	printf("</html>\n");

}

void print_conf_2(char *path, char *name, char *operation){
	printf("Content-type: text/html\n\n");
    printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de Archivos</title>\n");
    printf("</head>\n");
    printf("<body background='/static/fondo.png'>\n");	
	printf("<h1 style=font-family:verdana;color:black;>Explorador de Archivos</h1><br>");
	char path_nuevo[2048];
	strcpy(path_nuevo,path);	
	char ultimo3[1024];//dato para acceder ala penultima carpeta
	get_path_anterior(path_nuevo,ultimo3);// penultima ruta
	printf("<h2 style=font-style:oblique;color:black;>Directorio actual: %s<br>",path_nuevo);
	printf("<br>");
	listar_datos(path_nuevo);
	printf("</body>\n");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("<td><img alt="" height='40' src='/static/home.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/index.html\" style=font-family:verdana;color:black;>Portada</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");

}

void print_form(char *dato, char *ruta){ 
        printf("<form action=\"/cgi-bin/script_4.cgi\" method=\"post\">\n");
		//Agregar datos ocultos al formulario	
		int j;
		int k = 0;	
		int tam = strlen(ruta);
		char new_ruta[tam];

		if(ruta[tam-1] == '/')ruta[tam-1] = '\0';
		for(j = 0; j < tam; j++){
			if(ruta[j] != '.'){
				new_ruta[k] = ruta[j];
				k++;
				}
			}
		new_ruta[tam] = '\0';
		//**
		int m = tam;
		k = 0;
		int r;
		int flag = 0;
		char nivel_atras[tam];
		int x;
		if(strcmp(dato,"..")==0){
			while(flag == 0){
				if(new_ruta[m] == '/'){
					for(r = m; r >= 0; r--){
						k++;
						}
						flag = 1;
					}
				m--;
				}
			for(x = 0; x < k; x++){
				nivel_atras[x] = new_ruta[x];
			}
		}
		nivel_atras[x] = '\0';
		int tam2 = strlen(new_ruta);
		if(strcmp(dato,"..")==0)
			printf("<input type=\"hidden\" value=\"%s\" name=\"path\">\n",nivel_atras);
		else{
			strcat(new_ruta,"/");
			strcat(new_ruta,dato);
			printf("<input type=\"hidden\" value=\"%s\" name=\"path\">\n",new_ruta);
		}
		printf("<input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",dato);
		//Fin Agregacion de datos ocultos
		
		if (strstr(dato, ".") != NULL ) {
			if(strcmp(dato, "..") == 0)
				printf("<h6>Directorio: %s</h6>",dato);
			else printf("<h6>Archivo: %s</h6>",dato);
		}else printf("<h6>Directorio: %s</h6>",dato);
		//printf("<br>");
		printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
        printf("<tr>\n");
        if (strstr(dato, ".") != NULL) {
			if(strcmp(dato, "..") == 0) // es directorio
				printf("<td><img alt="" height='40' src='/static/directorio.png' width='40' align='middle'/></td>\n");
			else printf("<td><img alt="" height='40' src='/static/fichero.png' width='40' align='middle'/></td>\n");
		}else printf("<td><img alt="" height='40' src='/static/directorio.png' width='40' align='middle'/></td>\n");
        printf("<td><center><h4 style='color:black'>/%s</h4></td></center>\n",dato);
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"1\">Abrir</td>");
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"2\">Eliminar</td>");
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"5\">Mover</td>");	
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"3\">Copiar</td>");	
        if (strstr(dato, ".") == NULL) 
			printf("<td><input type=\"radio\" name=\"operacion\" value=\"4\">Crear archivo o directorio</td>");
        printf("<td><input type=\"submit\" value=\"OK\">\n");
        printf("</tr>\n");
        printf("</table>\n");
        printf("</form>\n");
}


void separar(char *cadena, char *linea, char separador){
    int x, y; 
    x = 0; y = 0;   
    while ((linea[x]) && (linea[x] != separador)){
		cadena[x] = linea[x];
		x = x + 1;
    }
    cadena[x] = '\0';
    if (linea[x]) ++x;
    y = 0; 
    while (linea[y] = linea[x]){
		linea[y] = linea[x];
		y++;
		x++;
    }
}

void reemplazar(char *cadena, char *resultado){
    char *separador = "%2F";
    char *remplazo = "/";
    int i =0;
    int j =0;
    for(i=0;i<(strlen(cadena));i++){
        if( (cadena[i] == separador[0]) && (cadena[i+1] == separador[1]) && (cadena[i+2] == separador[2]) ){
                resultado[j] = '/';
                i = i+2;
        }else{
            resultado[j] = cadena[i];
        }
        j = j+1;
    }
    resultado[j] = '\0';
}

void last_clean(char *var1, char *var2){ 
    char tmp[] = "/";
    strcat(tmp,var2);
    char *p;
    p = strstr(var1, tmp);
    int i ;
    int lim = (p-var1);
    char res[1024];
    for(i=0;i<lim;i++)
        res[i] = var1[i];
    res[i] = '\0';
    strcpy(var1,res);
}

void get_path_anterior(char *ruta, char *last){
    int tam = strlen(ruta)-1;
    int pos = 0;
    int puntero = 0;
    for(pos=tam;pos>=0;pos--){
        if(ruta[pos]=='/'){
            puntero = pos;
            pos = -6;
        }
    }
    int i = 0;
    char ruta2[1024];
    for(i;i<puntero;i++){
        ruta2[i]=ruta[i];
    }
    ruta2[i]='\0';
    char last2[1024];
    int j = puntero+1;
    int k = 0;
    for(j;j<strlen(ruta);j++){
        last2[k]=ruta[j];
        k++;
    }
    last2[k] = '\0';
    strcpy(ruta,ruta2);
    strcpy(last,last2);
}


void listar_datos(char *ruta){
	DIR *dir;
    struct dirent *mydirent;
    int i = 1;    
    if((dir = opendir(ruta)) == NULL)
		err_quit("opendir");
    while((mydirent = readdir(dir)) != NULL){ 	
		char *name = mydirent->d_name;
		print_form(name, ruta);
    }
    closedir(dir);
    //exit(EXIT_SUCCESS);
}

void err_quit(char *msg){
    perror(msg);
    exit(EXIT_FAILURE);
}
