#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
/*
 * Explorador de archivos en C
 * autor: Mauricio Trigo Uriona
 * */
void print_html(char *dato);
void print_form(char *dato, char *ruta);
void err_quit(char *msg);
void listar_datos(char *ruta);

int main(){
    print_html("/");	
    return 0;
}

void print_html(char *dato){
	printf("Content-type: text/html\n\n");
    printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de archivos</title>\n");
    printf("</head>\n");
    printf("<body background='/static/fondo.png'>\n");	
	printf("<h1 style=font-family:verdana;color:black;>Explorador de Archivos</h1><br>");
	printf("<h2 style=font-style:oblique;color:black;>Directorio actual: %s",dato);	
	printf("<br>");
	listar_datos(dato);
    printf("</body>\n");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("<td><img alt="" height='40' src='/static/home.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/index.html\" style=font-family:verdana;color:black;>Portada</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");

}

void print_form(char *dato, char *ruta){ 
        printf("<form action=\"/cgi-bin/script_4.cgi\" method=\"post\">\n");
		//datos ocultos	
		if(strcmp(ruta,"/")==0)
			printf("<input type=\"hidden\" value=\"%s%s\" name=\"path\">\n",ruta,dato);
		else
			printf("<input type=\"hidden\" value=\"%s/%s\" name=\"path\">\n",ruta,dato);
		
		printf("<input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",dato);
		//datos ocultos
		
		if (strstr(dato, ".") !=NULL ) {
			if(strcmp(dato, "..") == 0)
				printf("<h6>Directorio: %s</h6>",dato);
			else printf("<h6>Archivo: %s</h6>",dato);
		}else printf("<h6>Directorio: %s</h6>",dato);
		//printf("<br>");
		printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
        printf("<tr>\n");
        if (strstr(dato, ".") != NULL) {
			if(strcmp(dato, "..") == 0) // es directorio
				printf("<td><img alt="" height='40' src='/static/directorio.png' width='40' align='middle'/></td>\n");
			else printf("<td><img alt="" height='40' src='/static/fichero.png' width='40' align='middle'/></td>\n");
		}else printf("<td><img alt="" height='40' src='/static/directorio.png' width='40' align='middle'/></td>\n");
        printf("<td><center><h4 style='color:black'>/%s</h4></td></center>\n",dato);
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"1\">Abrir</td>");
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"2\">Eliminar</td>");
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"5\">Mover</td>");	
        printf("<td><input type=\"radio\" name=\"operacion\" value=\"3\">Copiar</td>");	
        if (strstr(dato, ".") == NULL) 
			printf("<td><input type=\"radio\" name=\"operacion\" value=\"4\">Crear archivo o directorio</td>");
        printf("<td><input type=\"submit\" value=\"OK\">\n");
        printf("</tr>\n");
        printf("</table>\n");
        printf("</form>\n");
}


void listar_datos(char *ruta){
	DIR *dir;
    struct dirent *mydirent;
    int i = 1;    
    
    if((dir = opendir(ruta)) == NULL)
		err_quit("opendir");
    while((mydirent = readdir(dir)) != NULL){ 	
		char *name = mydirent->d_name;
		print_form(name, ruta);
    }
    closedir(dir);
}

void err_quit(char *msg){
    perror(msg);
    exit(EXIT_FAILURE);
}
