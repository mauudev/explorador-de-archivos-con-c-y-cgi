#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#define MAXLEN 1024
/*
 * Explorador de archivos en C
 * autor: Mauricio Trigo Uriona
 * */

void reemplazar(char *cadena, char *resultado);
void separar(char *cadena, char *linea, char separador){
    int x, y;
    x = 0; y = 0;
    while ((linea[x]) && (linea[x] != separador)){
	cadena[x] = linea[x];
	x = x + 1;
    }
    cadena[x] = '\0';
    if (linea[x]) ++x;
    y = 0;   
    while (linea[y] = linea[x]){
		linea[y] = linea[x];
		y++;
		x++;
    }
}

int main(void){
	FILE *fptr;
    char *lenstr;
    char inputBuffer[MAXLEN];
    int contentLength;
    int i;
    char x;
    char path[200];
    char namefd[100];
    char destino[200];


    printf ("Content-type:text/html\n\n");
    printf("<TITLE>Crear archivo o directorio</TITLE>\n");
    lenstr = getenv("CONTENT_LENGTH");

    if (lenstr!= NULL)
		contentLength = atoi(lenstr);   
    else contentLength = 0;
    if (contentLength > sizeof(inputBuffer)-1)
		contentLength = sizeof(inputBuffer)-1;

    i = 0;

    while (i < contentLength){
		x = fgetc(stdin);
		if (x==EOF) break;
			inputBuffer[i] = x;
		i++;
    }

    inputBuffer[i] = '\0';
    contentLength = i;
    
    separar(path, inputBuffer, '=');
    separar(path, inputBuffer, '&');
    
    separar(namefd, inputBuffer, '=');
    separar(namefd, inputBuffer, '&');
    
    separar(destino, inputBuffer, '=');
    separar(destino, inputBuffer, '&');
    
    
    char path2[200];
	reemplazar(path, path2);
    
    destino[i] = '\0';
	int j;
	for (j = 0 ; destino[j] != '\0' ; j++ ){
         if (destino[j] == '+')
              destino[j] = ' ';
    }
 //************ INICIO ESTO CREA ARCHIVOS Y CARPETAS EN EL DIRECTORIO CGI-BIN
    //if(strstr(destino, ".") != NULL){
		//fptr = fopen(destino, "w");
		//if (fptr == NULL){
			//printf("File does not exists \n");
			//return 0;
		//}
		//fprintf(fptr, "Nombre de archivo: %s\n", destino);
		//fclose(fptr);
		//printf("<h3>Archivo creado correctamente! '%s'",destino);
	//}else {
		 //if (mkdir(destino, 0777)){
			//perror("mkdir");                   
			//exit(EXIT_FAILURE);
		//}
		//printf("<h3>Directorio creado correctamente! '%s'",destino);
    //}
 //************ FIN CREA ARCHIVOS Y CARPETAS EN EL DIRECTORIO CGI-BIN
 
 //************ INICIO CREA ARCHIVOS Y CARPETAS EN EL DIRECTORIO ESPECIFICADO EN FORMULARIO
	char file_name[2048];
	char destino2[200];
	if(strstr(destino, ".") != NULL){ // es un fichero
		char touch[200] = "touch '";
		int i;
		for(i = 0; destino[i] != '\0'; i++){
			if(destino[i] == ' ')
				destino2[i] = '_';
			else destino2[i] = destino[i];
			}
		destino2[i] = '\0';
		strcat(touch,path2);
		strcat(touch,"/");
		strcat(touch,destino2);
		strcat(touch,"'");	
		strcat(touch,"\0");	
		system(touch);
	}else{ // es directorio
		char mkdir[200] = "mkdir ";
		int i;
		for(i = 0; destino[i] != '\0'; i++){
			if(destino[i] == ' ')
				destino2[i] = '_';
			else destino2[i] = destino[i];
			}
		destino2[i] = '\0';
		strcat(mkdir,path2);
		strcat(mkdir,"/");
		strcat(mkdir,destino2);
		strcat(mkdir,"\0");	
		system(mkdir);
		}
		strcpy(file_name,destino2);
 //************ FIN CREA ARCHIVOS Y CARPETAS EN EL DIRECTORIO ESPECIFICADO EN FORMULARIO
 
//************** PRINT HTML******************
    printf("<html>\n");
    printf("<head>\n");
    printf("<link rel='icon' type='image/png' href='/static/favicon.png' />\n");
    printf("<title>Explorador de archivos</title>\n");
    printf("</head>\n");
    printf("<body background='/static/fondo.png'>\n");	
    printf("<br>");    
	printf("<img alt="" height='40' src='/static/sucess.png' width='40' align='middle'/><a style=color:black;font-style:oblique;font-size:x-large;><strong>Se ha creado el elemento '%s' correctamente.. </strong></a><br>\n",destino);
    printf("<a style=color:white;font-size:large;font-size:oblique;><form action=\"/cgi-bin/script_4.cgi\" method=\"post\">\n");
    printf("<a style=color:white;font-size:large;font-size:oblique;><input type=\"hidden\" value=\"%s\" name=\"path\">\n",path2);
	printf("<a style=color:white;font-size:large;font-size:oblique;><input type=\"hidden\" value=\"%s\" name=\"namefd\">\n",file_name);
    printf("<a style=color:white;font-size:large;font-size:oblique;><input type=\"hidden\" value=\"1\" name=\"operacion\">\n");
    printf("<br>");  
    printf("<a style=color:white;font-size:large;font-size:oblique;><input type=\"submit\" value=\"Regresar al directorio\">\n");
    printf("</form>\n");
    printf("</body>\n");
    printf ("<br>");
    printf("<br>");
    printf("<TABLE BORDER='3' CELLSPACING='5'> \n");   
    printf("<tr>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_2.cgi\" style=font-family:verdana;color:black;>Ir al directorio raiz</a></td>\n");
    printf("<td><img alt="" height='40' src='/static/iniciar_en.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/cgi-bin/script_1.cgi\" style=font-family:verdana;color:black;>Ir al directorio home</a></td>\n");
	printf("<td><img alt="" height='40' src='/static/home.png' width='40' align='middle'/><a name=ATRAS type=button href=\"/index.html\" style=font-family:verdana;color:black;>Portada</a></td>\n");
	printf("</tr>\n");
    printf("</table>\n");
    printf("</html>\n");
	

//************** END OF PRINT ***************
    return 0;
}

void reemplazar(char *cadena, char *resultado){
    char *separador = "%2F";
    char *remplazo = "/";
    int i =0;
    int j =0;
    for(i=0;i<(strlen(cadena));i++){
        if( (cadena[i] == separador[0]) && (cadena[i+1] == separador[1]) && (cadena[i+2] == separador[2]) ){
                resultado[j] = '/';
                i = i+2;
        }else{
            resultado[j] = cadena[i];
        }
        j = j+1;
    }
    resultado[j] = '\0';
}
